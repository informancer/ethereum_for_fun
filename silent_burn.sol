// SilentBurn (c) informancer, 2016
//
// This file is part of Ethereum for fun project.
//
// To the extent possible under law, the author(s) have dedicated all
// copyright and related and neighboring rights to this software to
// the public domain worldwide. This software is distributed without
// any warranty.
//
// You should have received a copy of the CC0 Public Domain Dedication
// along with this software.
// If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
//

// It contract just burn ether. Everyone can send any amount of ether to it and nobody can take it. I hope you find it useful.

contract SilentBurn {

    // There is no code in the contract in purpose to decrease gas spending.
  
}
